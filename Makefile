# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/30 14:57:31 by ishyian           #+#    #+#              #
#    Updated: 2019/06/22 11:08:25 by ishyian          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC =				gcc
NAME_CHECKER =		checker
NAME_PUSH_SWAP =	push_swap
CFLAGS =			-Wall -Wextra -Werror
SRCS =				./src/set_params.c \
					./src/error_processor.c \
					./src/parse_flags.c \
					./src/get_stack.c \
					./src/instruction_processor.c \
					./src/pa_pb.c \
					./src/ra_rb_rr.c \
					./src/rra_rrb_rrr.c \
					./src/sa_sb_ss.c \
					./src/is_stack_sorted.c \
					./src/compare_items.c \
					./src/print_stack.c \
					./src/display_stack.c \
					./src/init_destroy_graphic.c \
					./src/get_max_min_stack_item.c \
					./src/linear_gradient.c \
					./src/mlx_img.c \
					./src/global.c \
					./src/check_usage.c

SRCS_CHECKER =		./src/checker.c \
					./src/get_instructions.c

SRCS_PUSH_SWAP =	./src/push_swap.c \
					./src/stack_sorter.c \
					./src/stack_sorter_stack_id.c \
					./src/stack_sorter_get_median.c \
					./src/stack_sorter_divide.c \
					./src/stack_sorter_rotate.c \
					./src/stack_sorter_swap.c \
					./src/stack_sorter_push_to_a.c \
					./src/compare_item_and_median.c \
					./src/instructions_to_fd.c

OBJ =				$(SRCS:.c=.o)
OBJ_CHECKER =		$(SRCS_CHECKER:.c=.o)
OBJ_PUSH_SWAP =		$(SRCS_PUSH_SWAP:.c=.o)
LIBS = 				-lft -lncurses -lmlx -framework OpenGL -framework Appkit
LIB_DIR =			./libft/
LIB_FT =			./libft/libft.a
INCLUDES =			./inc/push_swap.h

all: 				$(NAME_CHECKER) $(NAME_PUSH_SWAP)

$(NAME_CHECKER):	$(LIB_FT) $(OBJ) $(OBJ_CHECKER)
					$(CC) -o $(NAME_CHECKER) $(OBJ) $(OBJ_CHECKER) $(LIBS) -L $(LIB_DIR)

$(NAME_PUSH_SWAP):	$(LIB_FT) $(OBJ) $(OBJ_PUSH_SWAP)
					$(CC) -o $(NAME_PUSH_SWAP) $(OBJ) $(OBJ_PUSH_SWAP) $(LIBS) -L $(LIB_DIR)

$(OBJ): 			%.o: %.c $(INCLUDES)
					$(CC) $(CFLAGS) -I $(INCLUDES) -o $@ -c $<

$(OBJ_CHECKER): 	%.o: %.c $(INCLUDES)
					$(CC) $(CFLAGS) -I $(INCLUDES) -o $@ -c $<

$(OBJ_PUSH_SWAP): 	%.o: %.c $(INCLUDES)
					$(CC) $(CFLAGS) -I $(INCLUDES) -o $@ -c $<

$(LIB_FT):			libft
					make -C $(LIB_DIR)

libft:
					make -C $(LIB_DIR)

clean:
					rm -f $(OBJ) $(OBJ_CHECKER) $(OBJ_PUSH_SWAP)
					make clean -C $(LIB_DIR)

fclean: 			clean
					rm -f $(NAME_CHECKER) $(NAME_PUSH_SWAP)
					make fclean -C $(LIB_DIR)

re: 				fclean all

.PHONY: 			all libft clean fclean

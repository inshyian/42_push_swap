/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 11:53:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 09:59:08 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H
# define VALID_INSTRUCTIONS "pa pb sa sb ss ra rb rr rra rrb rrr"
# define VALID_FLAGS "isovgdh"
# define MLX_WIN_WIDTH 800
# define MLX_WIN_HEIGHT 600
# define VIS_BEGIN_END_PAUSE 3000000
# define ASCENDING 1
# define SX(x) ((x) == 0 ? "sa" : "sb")
# define PX(x) ((x) == 0 ? "pa" : "pb")
# define RX(x) ((x) == 0 ? "ra" : "rb")
# define RRX(x) ((x) == 0 ? "rra" : "rrb")
# define THIS(x) ((x) == 0 ? 0 : 1)
# define OTHER(x) ((x) == 0 ? 1 : 0)
# define G global()
# define MLX_WIN_NAME "Display stack"
# include <stdlib.h>
# include <ncurses.h>
# include "../libft/libft.h"

/*
** if ASCENDING == 1, sort in ascending order; 0 - sort in descending order
** flag -i <instructions_input_file>
** flag -s <stack_input_file>
** flag -o <instructions_output_file>
** flag -v enables ncurses visualization
** flag -g enables mlx visualization
** flag -d with -v or/and -g sets delay beetween instruction processing,
** default is 30000 ms
*/

typedef struct			s_params
{
	int					fd_iin;
	int					fd_sin;
	int					fd_iout;
	int					ascending;
	int					verbose;
	int					graphic;
	int					delay;
	int					wait;
	int					usage;
}						t_params;

typedef struct			s_div_n_con
{
	t_dllist			*first;
	t_dllist			*last;
	t_dllist			*first_;
	t_dllist			*last_;
	size_t				count;
}						t_div_n_con;

typedef struct			s_mlx
{
	int					width;
	int					height;
	void				*mlx_ptr;
	void				*win_ptr;
	void				*img_ptr;
	char				*img_data;
	int					bpp;
	int					size_line;
	int					endian;
	char				*max_stack_item;
	char				*min_stack_item;
	char				*negative_offset;
	int					line_height;
}						t_mlx;

typedef struct			s_point
{
	int					x;
	int					y;
	int					color;
}						t_point;

typedef struct			s_ncurses
{
	WINDOW				*win_stack_a;
	WINDOW				*win_stack_b;
	WINDOW				*win_instr;
	short				rows;
}						t_ncurses;

typedef struct			s_global
{
	t_dllist			*stacks[2];
	struct s_params		params;
	struct s_mlx		mlx;
	struct s_ncurses	ncurses;
}						t_global;

void					set_params(void);
void					error_processor(char *str, int code);
int						parse_flags(char **av, int ac);
void					get_stack(int ac, char **av, int i);
t_list					*get_instructions(void);
void					instruction_processor(t_list *instructions);
void					pa_pb(t_dllist **a, t_dllist **b, char *instruction);
void					sa_sb_ss(t_dllist **a, t_dllist **b, char *instruction);
void					ra_rb_rr(t_dllist **a, t_dllist **b, char *instruction);
void					rra_rrb_rrr(t_dllist **a,
									t_dllist **b,
									char *instruction);
void					print_stack(t_list *instruction,
									int mode);
int						is_stack_sorted(void);
void					stack_sorter(void);
int						stack_sorter_stack_id(t_dllist *first);
char					*stack_sorter_get_median(t_dllist *first,
													t_dllist *last);
void					stack_sorter_divide(t_div_n_con *divide_conqure,
											t_list **all_instructions);
void					stack_sorter_rotate(t_div_n_con *div_con,
											int curr_stack,
											t_list **all_instructions);
void					stack_sorter_swap(t_dllist **first, t_dllist **second,
											t_list **all_instructions);
void					stack_sorter_push_to_a(t_dllist *first, t_dllist *last,
												t_list **all_instructions);
int						compare_items(t_dllist *a, t_dllist *b);
int						compare_item_and_median(t_dllist *a, char *median);
void					instructions_to_fd(t_list *instructions);
void					display_stacks(void);
void					mlx_img_put_pixel(int x, int y, int color);
void					mlx_img_clear(void);
void					init_visualization(t_list *instructions);
void					destroy_visualization(t_list *instructions);
char					*get_max_stack_item(t_dllist *stack);
char					*get_min_stack_item(t_dllist *stack);
int						linear_gradient(int percent, int start_c, int end_c);
t_global				*global(void);
void					usage(int ac);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_logfile_fd.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/20 17:39:21 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 14:07:14 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>

int					ft_get_logfile_fd(void)
{
	static int		fd;

	if (!fd)
		fd = open("log", O_CREAT | O_RDWR, 0775);
	return (fd);
}

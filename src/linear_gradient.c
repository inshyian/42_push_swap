/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   linear_gradient.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 14:12:47 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 12:36:45 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

static int		get_light(int start_c, int end_c, float percentage)
{
	return ((int)((1 - percentage) * start_c + percentage * end_c));
}

int				linear_gradient(int curr_x, int start_c, int end_c)
{
	int			red;
	int			green;
	int			blue;
	float		percentage;

	if (curr_x > (G->mlx.width / 2))
		curr_x = curr_x - (G->mlx.width / 2);
	percentage = curr_x / (float)(G->mlx.width / 2);
	red = get_light((start_c >> 16) & 0xFF, (end_c >> 16) & 0xFF, percentage);
	green = get_light((start_c >> 8) & 0xFF, (end_c >> 8) & 0xFF, percentage);
	blue = get_light(start_c & 0xFF, end_c & 0xFF, percentage);
	return ((red << 16) | (green << 8) | blue);
}

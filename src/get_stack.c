/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_stack.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 14:28:11 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 23:36:32 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

static int			search_dup(char *item, t_dllist *list)
{
	while (list)
	{
		if (ft_longexprcmp(item, '=', list->content))
			return (1);
		list = list->next;
	}
	return (0);
}

static void			to_error_processor(char **items, t_dllist *list, int i,
																	int code)
{
	char		*item;

	item = ft_strdup(items[i]);
	i = 0;
	while (items[i])
		free(items[i++]);
	free(items);
	ft_dllstdel(&list, &ft_memsdel);
	error_processor(item, code);
}

static void			stack_item_validator(char *item, t_dllist *list,
											char **items, int i)
{
	int		ii;

	ii = 0;
	if (!item[0])
		to_error_processor(items, list, i, 13);
	if (ft_issign(item[0]) && item[1])
		ii++;
	while (item[ii])
	{
		if (!ft_isdigit(item[ii]) && item[ii] != '.')
			to_error_processor(items, list, i, 11);
		ii++;
	}
	if (search_dup(item, list))
		to_error_processor(items, list, i, 12);
}

static void			parse_item(char *item, t_dllist *curr, t_dllist **first)
{
	char		**items;
	t_dllist	*new;
	int			i;

	i = 0;
	items = ft_strsplit(item, ' ');
	free(item);
	while (items[i])
	{
		stack_item_validator(items[i], *first, items, i);
		new = ft_dllstnew(items[i], ft_strlen(items[i]) + 1);
		new->next = NULL;
		ft_dllstaddend(&curr, new);
		if (!*first)
			*first = curr;
		else
			curr = curr->next;
		i++;
	}
	i = 0;
	while (items[i])
		free(items[i++]);
	free(items);
}

void				get_stack(int ac, char **av, int i)
{
	t_dllist	*list;
	char		*item;

	list = NULL;
	if (!G->params.fd_sin)
	{
		if (i > (ac - 1))
			error_processor(NULL, 10);
		while (i <= (ac - 1))
		{
			item = ft_strdup(av[i]);
			parse_item(item, list, &list);
			i++;
		}
	}
	else
		while (get_next_line(G->params.fd_sin, &item) == 1)
			parse_item(item, list, &list);
	if (ft_dllstlen(list, NULL) == 0)
		error_processor(NULL, 10);
	ft_dllstaddend(&list, list);
	G->stacks[0] = list;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compare_item_and_median.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/13 11:57:04 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 09:38:07 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

int			compare_item_and_median(t_dllist *a, char *median)
{
	if (ASCENDING == 1)
	{
		if (ft_longexprcmp(a->content, '>', median))
			return (1);
		else
			return (0);
	}
	else
	{
		if (ft_longexprcmp(a->content, '<', median))
			return (1);
		else
			return (0);
	}
}

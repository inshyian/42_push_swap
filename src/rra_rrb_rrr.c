/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rra_rrb_rrr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 17:39:04 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 12:07:46 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

static void		shift_down(t_dllist **x)
{
	if (*x)
		*x = (*x)->prev;
}

void			rra_rrb_rrr(t_dllist **a, t_dllist **b, char *instruction)
{
	if (!ft_strcmp(instruction, "rra"))
		shift_down(a);
	else if (!ft_strcmp(instruction, "rrb"))
		shift_down(b);
	else if (!ft_strcmp(instruction, "rrr"))
	{
		shift_down(a);
		shift_down(b);
	}
}

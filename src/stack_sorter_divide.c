/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_sorter_divide.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/15 19:37:54 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 09:38:56 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

static void		check_rotate(t_div_n_con *div_con, int curr_stack,
								t_list **all_instructions)
{
	div_con->first = div_con->last;
	if (div_con->count)
		stack_sorter_rotate(div_con, curr_stack, all_instructions);
}

static void		updatefirst_last(t_div_n_con *div_con)
{
	div_con->count++;
	if (div_con->first == div_con->last)
		div_con->first = NULL;
	else
		div_con->first = div_con->first->next;
}

static void		updatefirst_last_(t_div_n_con *div_con)
{
	if (!div_con->last_)
		div_con->last_ = div_con->first;
	else
		div_con->first_ = div_con->first;
	if (div_con->first == div_con->last)
	{
		div_con->last = div_con->last->prev;
		div_con->first = NULL;
	}
	else
		div_con->first = div_con->first->next;
}

void			stack_sorter_divide(t_div_n_con *div_con,
									t_list **all_instructions)
{
	t_list		*next_instruction;
	char		*median;
	int			curr_stack;

	curr_stack = stack_sorter_stack_id(div_con->first);
	median = stack_sorter_get_median(div_con->first, div_con->last);
	while (div_con->first)
	{
		if ((curr_stack == 0 && compare_item_and_median(div_con->first, median))
		||
		(curr_stack == 1 && !compare_item_and_median(div_con->first, median)))
		{
			next_instruction = ft_lstnew(RX(THIS(curr_stack)), 3);
			updatefirst_last(div_con);
		}
		else
		{
			next_instruction = ft_lstnew(PX(OTHER(curr_stack)), 3);
			updatefirst_last_(div_con);
		}
		instruction_processor(next_instruction);
		ft_lstaddend(all_instructions, next_instruction);
	}
	free(median);
	check_rotate(div_con, curr_stack, all_instructions);
}

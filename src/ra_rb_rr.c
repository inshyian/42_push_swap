/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ra_rb_rr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 17:38:58 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/15 17:42:38 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

static void		shift_up(t_dllist **x)
{
	if (*x)
		*x = (*x)->next;
}

void			ra_rb_rr(t_dllist **a, t_dllist **b, char *instruction)
{
	if (!ft_strcmp(instruction, "ra"))
		shift_up(a);
	else if (!ft_strcmp(instruction, "rb"))
		shift_up(b);
	else if (!ft_strcmp(instruction, "rr"))
	{
		shift_up(a);
		shift_up(b);
	}
}

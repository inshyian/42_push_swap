/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_sorter_stack_id.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/19 21:22:41 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 12:08:05 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

int			stack_sorter_stack_id(t_dllist *first)
{
	if (G->stacks[0] == first)
		return (0);
	else
		return (1);
}

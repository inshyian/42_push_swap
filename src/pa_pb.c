/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pa_pb.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 17:38:51 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 11:37:36 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

static void		check_from(t_dllist **from)
{
	if ((*from)->next != *from)
	{
		(*from)->prev->next = (*from)->next;
		(*from)->next->prev = (*from)->prev;
		(*from) = (*from)->next;
	}
	else
		*from = NULL;
}

static void		push_stack(t_dllist **in, t_dllist **from)
{
	t_dllist	*item;

	item = *from;
	check_from(from);
	if (*in)
	{
		ft_dllstins((*in)->prev, item, *in);
		*in = item;
	}
	else
	{
		ft_dllstadd(in, item);
		ft_dllstaddend(in, *in);
	}
}

void			pa_pb(t_dllist **a, t_dllist **b, char *instruction)
{
	if (!ft_strcmp(instruction, "pa"))
	{
		if (*b)
			push_stack(a, b);
	}
	else if (!ft_strcmp(instruction, "pb"))
	{
		if (*a)
			push_stack(b, a);
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sa_sb_ss.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 17:38:41 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/19 13:42:28 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

static void			to_swap(t_dllist **x, t_dllist *next)
{
	ft_dllstswap(x, &next);
}

void				sa_sb_ss(t_dllist **a, t_dllist **b, char *instruction)
{
	if (!ft_strcmp(instruction, "sa"))
	{
		if (*a && (*a)->next != *a)
			to_swap(a, (*a)->next);
	}
	else if (!ft_strcmp(instruction, "sb"))
	{
		if (*b && (*b)->next != *b)
			to_swap(b, (*b)->next);
	}
	else if (!ft_strcmp(instruction, "ss"))
	{
		if (*a && (*a)->next != *a)
			to_swap(a, (*a)->next);
		if (*b && (*b)->next != *b)
			to_swap(b, (*b)->next);
	}
}

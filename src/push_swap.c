/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 14:29:27 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 10:25:57 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

int			main(int ac, char **av)
{
	int			i;

	set_params();
	i = parse_flags(av, ac);
	usage(ac);
	get_stack(ac, av, i);
	if (G->params.graphic || G->params.verbose)
		init_visualization(NULL);
	if (!is_stack_sorted())
		stack_sorter();
	if (G->stacks[0])
		ft_dllstdel(&G->stacks[0], &ft_memsdel);
	if (G->stacks[1])
		ft_dllstdel(&G->stacks[1], &ft_memsdel);
	if (G->params.graphic || G->params.verbose)
		destroy_visualization(NULL);
	return (0);
}

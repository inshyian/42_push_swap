/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions_to_fd.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/17 11:42:48 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 12:07:24 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

void			instructions_to_fd(t_list *instructions)
{
	while (instructions)
	{
		ft_fprintf(G->params.fd_iout, "%s\n", instructions->content);
		instructions = instructions->next;
	}
}

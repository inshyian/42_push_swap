/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_img.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/16 22:59:27 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 12:36:14 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

void			mlx_img_clear(void)
{
	ft_memset(G->mlx.img_data, 0, MLX_WIN_HEIGHT * MLX_WIN_WIDTH * 4);
}

/*
** 	(G->mlx.img_data + to_zero_byte)[2] = (color >> 16) & 0xFF;
** 	(G->mlx.img_data + to_zero_byte)[1] = (color >> 8) & 0xFF;
** 	(G->mlx.img_data + to_zero_byte)[0] = color & 0xFF;
*/

void			mlx_img_put_pixel(int x, int y, int color)
{
	int		to_zero_byte;

	to_zero_byte = x * 4 + G->mlx.size_line * y;
	*(int *)(G->mlx.img_data + to_zero_byte) = color;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instruction_processor.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 17:22:54 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 12:36:27 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "../inc/push_swap.h"

static void		interpretate(char *instruction)
{
	if (!ft_strcmp(instruction, "pa") ||
		!ft_strcmp(instruction, "pb"))
		pa_pb(&G->stacks[0], &G->stacks[1], instruction);
	else if (!ft_strcmp(instruction, "ra") ||
			!ft_strcmp(instruction, "rb") ||
			!ft_strcmp(instruction, "rr"))
		ra_rb_rr(&G->stacks[0], &G->stacks[1], instruction);
	else if (!ft_strcmp(instruction, "rra") ||
			!ft_strcmp(instruction, "rrb") ||
			!ft_strcmp(instruction, "rrr"))
		rra_rrb_rrr(&G->stacks[0], &G->stacks[1], instruction);
	else if (!ft_strcmp(instruction, "sa") ||
			!ft_strcmp(instruction, "sb") ||
			!ft_strcmp(instruction, "ss"))
		sa_sb_ss(&G->stacks[0], &G->stacks[1], instruction);
}

static void		goer(t_list *instructions)
{
	while (instructions)
	{
		interpretate((char *)instructions->content);
		if (G->params.graphic)
			display_stacks();
		if (G->params.verbose)
			print_stack(instructions, 0);
		if (G->params.graphic || G->params.verbose)
			usleep(G->params.delay);
		instructions = instructions->next;
	}
}

void			instruction_processor(t_list *instructions)
{
	goer(instructions);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_sorter_rotate.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/17 16:11:54 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 00:01:27 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

static void		stack_rotate(t_div_n_con *div_con, int curr_stack,
								t_list **all_instructions)
{
	t_list		*next_instruction;

	while (div_con->count--)
	{
		div_con->first = div_con->first->prev;
		next_instruction = ft_lstnew(RRX(THIS(curr_stack)), 4);
		ft_lstaddend(all_instructions, next_instruction);
		instruction_processor(next_instruction);
	}
}

void			stack_sorter_rotate(t_div_n_con *div_con, int curr_stack,
									t_list **all_instructions)
{
	div_con->first = div_con->first->next;
	if (ft_dllstlen(G->stacks[curr_stack], NULL) != div_con->count)
		stack_rotate(div_con, curr_stack, all_instructions);
	else
	{
		div_con->count = 0;
		div_con->first = G->stacks[curr_stack];
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_destroy_graphic.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 17:09:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 14:51:26 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <mlx.h>
#include "../inc/push_swap.h"

static void		destroy_mlx(void)
{
	mlx_destroy_window(G->mlx.mlx_ptr, G->mlx.win_ptr);
	free(G->mlx.mlx_ptr);
}

static void		init_mlx(int width, int height)
{
	G->mlx.width = width;
	G->mlx.height = height;
	G->mlx.mlx_ptr = mlx_init();
	G->mlx.win_ptr = mlx_new_window(G->mlx.mlx_ptr, width, height,
									MLX_WIN_NAME);
	G->mlx.img_ptr = mlx_new_image(G->mlx.mlx_ptr, width, height);
	G->mlx.img_data = mlx_get_data_addr(G->mlx.img_ptr, &G->mlx.bpp,
										&G->mlx.size_line, &G->mlx.endian);
}

void			destroy_visualization(t_list *instructions)
{
	usleep(G->params.wait);
	if (G->params.verbose)
		print_stack(instructions, 2);
	if (G->params.graphic)
		destroy_mlx();
}

static void		check_stack_len(void)
{
	if (G->params.graphic)
		if (ft_dllstlen(G->stacks[0], NULL) > (MLX_WIN_HEIGHT / 2 - 10))
		{
			G->params.graphic = 0;
			ft_printf("MLX visualization disabled. Too many stack items, \
count of stack items should be <= (MLX_WIN_HEIGHT / 2 - 10)\n");
		}
}

void			init_visualization(t_list *instructions)
{
	char		*tmp;

	check_stack_len();
	if (G->params.graphic)
	{
		init_mlx(MLX_WIN_WIDTH, MLX_WIN_HEIGHT);
		G->mlx.max_stack_item = ft_strdup(get_max_stack_item(G->stacks[0]));
		G->mlx.min_stack_item = get_min_stack_item(G->stacks[0]);
		G->mlx.min_stack_item = ft_longexpr(G->mlx.min_stack_item, '-', "1");
		if (ft_longexprcmp(G->mlx.min_stack_item, '<', "0"))
			G->mlx.negative_offset =
				ft_longexpr(G->mlx.min_stack_item, '*', "-1");
		else
			G->mlx.negative_offset = ft_itoa(0);
		tmp = G->mlx.max_stack_item;
		G->mlx.max_stack_item =
				ft_longexpr(G->mlx.max_stack_item, '+', G->mlx.negative_offset);
		free(tmp);
		G->mlx.line_height = G->mlx.height / ft_dllstlen(G->stacks[0], NULL);
		display_stacks();
	}
	if (G->params.verbose)
		print_stack(instructions, 1);
	if (G->params.graphic || G->params.verbose)
		usleep(G->params.wait);
}

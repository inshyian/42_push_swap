/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_stack_sorted.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 16:06:29 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 00:14:41 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

static int		is_stack_b_empty(void)
{
	if (G->stacks[1])
		return (0);
	return (1);
}

static int		goer(t_dllist *stack_a, t_dllist *first_a)
{
	while (stack_a && stack_a->next != first_a)
	{
		if (compare_items(stack_a, stack_a->next))
			return (0);
		stack_a = stack_a->next;
	}
	return (1);
}

int				is_stack_sorted(void)
{
	if (!is_stack_b_empty())
		return (0);
	return (goer(G->stacks[0], G->stacks[0]));
}

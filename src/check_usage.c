/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_usage.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/22 09:50:06 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 10:27:37 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

static void		process_usage(void)
{
	ft_printf("Usage:\t./push_swap \"stack_a\" | ./checker \"stack_a\"\n");
	ft_printf("\t-s <stack_input_file>\n");
	ft_printf("\t-i <instructions_input_file> (for checker)\n");
	ft_printf("\t-o <instructions_output_file> (for push_swap)\n");
	ft_printf("\t-v enables ncurses visualization\n");
	ft_printf("\t-g enables mlx visualization\n");
	ft_printf("\t-d <time_in_ms> with -v or/and -g sets delay beetween \
instruction processing, default is 30000 ms\n");
	ft_printf("\t-h displays this usage\n");
	ft_printf("\n\tExamples:\n");
	ft_printf("\t./push_swap \"20 2 1 13\" | ./checker \"20 2 1 13\"\n");
	ft_printf("\tOK\n");
	ft_printf("\t./push_swap -s file1 | ./checker -s file1\n");
	ft_printf("\tOK\n");
}

void			usage(int ac)
{
	if (G->params.usage || ac == 1)
	{
		process_usage();
		exit(0);
	}
}

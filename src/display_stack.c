/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_stack.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 17:11:18 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 12:35:06 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <mlx.h>
#include "../inc/push_swap.h"

static int		get_pcnt(char *item)
{
	char		*percent;
	char		*interim;
	char		*width;
	int			pcnt;

	interim = ft_longexpr(item, '+', G->mlx.negative_offset);
	item = interim;
	width = ft_itoa(MLX_WIN_WIDTH - MLX_WIN_WIDTH / 100);
	interim = ft_longexpr(item, '*', width);
	free(width);
	free(item);
	percent = ft_longexpr(interim, '/', G->mlx.max_stack_item);
	free(interim);
	pcnt = ft_atoi(percent);
	free(percent);
	return (pcnt);
}

static void		print_line(char *item, t_point p)
{
	int			line_len;
	int			col;
	int			i;
	int			ii;

	line_len = get_pcnt(item) / 2;
	ii = 0;
	while (ii < line_len)
	{
		col = linear_gradient(ii, 0x228b22, 0xff0000);
		i = 0;
		while (++i < G->mlx.line_height)
			mlx_img_put_pixel(p.x + ii, p.y + i, col);
		ii++;
	}
}

static void		print_lines(t_dllist *stack, t_point point)
{
	t_dllist	*first;

	first = stack;
	while (stack)
	{
		print_line(stack->content, point);
		point.y += G->mlx.line_height;
		stack = stack->next;
		if (stack == first)
			break ;
	}
}

void			display_stacks(void)
{
	t_point		a;
	t_point		b;

	a.x = 1;
	a.y = 1;
	b.x = G->mlx.width / 2 + 1;
	b.y = 1;
	if (G->stacks[0])
		print_lines(G->stacks[0], a);
	if (G->stacks[1])
		print_lines(G->stacks[1], b);
	mlx_put_image_to_window(G->mlx.mlx_ptr, G->mlx.win_ptr,
							G->mlx.img_ptr, 0, 0);
	mlx_do_sync(G->mlx.mlx_ptr);
	mlx_img_clear();
}

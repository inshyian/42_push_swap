/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_params.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 19:16:51 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 10:29:29 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

void		set_params(void)
{
	G->params.usage = 0;
	G->params.fd_sin = 0;
	G->params.fd_iin = 0;
	G->params.fd_iout = 1;
	G->params.ascending = ASCENDING;
	G->params.verbose = 0;
	G->params.graphic = 0;
	G->params.delay = 30000;
	G->params.wait = VIS_BEGIN_END_PAUSE;
	G->stacks[1] = NULL;
	G->stacks[0] = NULL;
}

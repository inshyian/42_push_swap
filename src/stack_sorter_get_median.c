/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_sorter_get_median.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/15 19:17:34 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/19 20:22:26 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

char		*stack_sorter_get_median(t_dllist *first, t_dllist *last)
{
	t_dllist	*dup;
	t_dllist	*middle;
	char		*value;

	dup = ft_dllstdup(first, last);
	ft_quicksort(&dup, NULL, &compare_items);
	middle = ft_dllstmiddle(dup, NULL);
	value = ft_strdup(middle->content);
	ft_dllstdel(&dup, &ft_memsdel);
	return (value);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_sorter.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/10 16:43:42 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 09:45:29 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

/*
** Function stack_sorter_check_sequence checks sequence,
** which group will process firstly and which secondarily
** (first - last or first_ - last_) to prevent overlapping of
** unprocessed group.
*/

static void		stack_sorter_check_sequence(t_div_n_con *div_con)
{
	if ((div_con->first_ == G->stacks[0] && div_con->first == G->stacks[1]))
	{
		ft_pswap((void **)&div_con->first, (void **)&div_con->first_);
		ft_pswap((void **)&div_con->last, (void **)&div_con->last_);
	}
}

static void		stack_sorter_(t_dllist *begin, t_dllist *end,
								t_list **all_instructions)
{
	t_div_n_con		div_con;
	int				curr_stack;

	ft_bzero(&div_con, sizeof(t_div_n_con));
	div_con.first = begin;
	div_con.last = end;
	curr_stack = stack_sorter_stack_id(div_con.first);
	if (ft_dllstlen(div_con.first, div_con.last) > 2)
		stack_sorter_divide(&div_con, all_instructions);
	stack_sorter_check_sequence(&div_con);
	if (div_con.first && ft_dllstlen(div_con.first, div_con.last) > 2)
		stack_sorter_(div_con.first, div_con.last, all_instructions);
	else if (ft_dllstlen(div_con.first, div_con.last) > 0)
	{
		stack_sorter_swap(&div_con.first, &div_con.last, all_instructions);
		stack_sorter_push_to_a(div_con.first, div_con.last, all_instructions);
	}
	if (div_con.first_ && ft_dllstlen(div_con.first_, div_con.last_) > 2)
		stack_sorter_(div_con.first_, div_con.last_, all_instructions);
	else if (ft_dllstlen(div_con.first_, div_con.last_) > 0)
	{
		stack_sorter_swap(&div_con.first_, &div_con.last_, all_instructions);
		stack_sorter_push_to_a(div_con.first_, div_con.last_, all_instructions);
	}
}

void			stack_sorter(void)
{
	t_list		*all_instructions;

	all_instructions = NULL;
	stack_sorter_(G->stacks[0], G->stacks[0]->prev, &all_instructions);
	instructions_to_fd(all_instructions);
	ft_lstdel(&all_instructions, &ft_memsdel);
}

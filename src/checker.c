/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 14:29:56 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 10:26:00 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

int			main(int ac, char **av)
{
	t_list		*instructions;
	int			i;

	instructions = NULL;
	set_params();
	i = parse_flags(av, ac);
	usage(ac);
	get_stack(ac, av, i);
	instructions = get_instructions();
	if (G->params.graphic || G->params.verbose)
		init_visualization(instructions);
	instruction_processor(instructions);
	if (G->params.graphic || G->params.verbose)
		destroy_visualization(instructions);
	ft_lstdel(&instructions, &ft_memsdel);
	if (is_stack_sorted())
		ft_printf("OK\n");
	else
		ft_printf("KO\n");
	return (0);
}

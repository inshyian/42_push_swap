/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_flags.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:33:40 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 11:04:19 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include "../inc/push_swap.h"

static void		check_flag(char **av, int *ii, int ac)
{
	char	*chrs;
	int		count;
	int		i;

	chrs = ft_strstrchrs(av[*ii], "o, i, s, d");
	count = ft_strlen(chrs);
	free(chrs);
	i = 0;
	while (av[*ii][i])
	{
		if (!ft_strchr(VALID_FLAGS, av[*ii][i]))
			error_processor(ft_strdup(av[*ii]), 30);
		i++;
	}
	if (count > 1)
		error_processor(ft_strdup(av[*ii]), 32);
	if (count && (*ii + 1) > ((ac - 1)))
		error_processor(ft_strdup(av[*ii]), 31);
}

static void		parse_options_more(char **av, int i, int *ii)
{
	if (*(av[i]) == 's')
	{
		G->params.fd_sin = open(av[++(*ii)], O_RDWR);
		if (G->params.fd_sin < 0 || ((read(G->params.fd_sin, 0, 0)) < 0))
			error_processor(ft_strdup(av[*ii]), 7);
	}
	if (*(av[i]) == 'd')
	{
		if (!ft_isnum(av[++(*ii)]) || ft_issign(av[(*ii)][0]))
			error_processor(ft_strdup(av[*ii]), 8);
		G->params.delay = ft_atoi(av[*ii]);
	}
	if (*(av[i]) == 'v')
		G->params.verbose = 1;
	if (*(av[i]) == 'g')
		G->params.graphic = 1;
	if (*(av[i]) == 'h')
		G->params.usage = 1;
}

static void		parse_options(char **av, int i, int *ii, int ac)
{
	check_flag(av, ii, ac);
	while (*(av[i]))
	{
		if (*(av[i]) == 'i')
		{
			G->params.fd_iin = open(av[++(*ii)], O_RDWR);
			if (G->params.fd_iin < 0 || ((read(G->params.fd_iin,
												0, 0)) < 0))
				error_processor(ft_strdup(av[*ii]), 5);
		}
		if (*(av[i]) == 'o')
		{
			G->params.fd_iout = open(av[++(*ii)], O_CREAT | O_RDWR,
											0775);
			if (G->params.fd_iout < 0 || ((read(G->params.fd_iout,
												0, 0)) < 0))
				error_processor(ft_strdup(av[*ii]), 6);
		}
		parse_options_more(av, i, ii);
		av[i]++;
	}
}

int				parse_flags(char **av, int ac)
{
	int		i;

	i = 1;
	while (i <= (ac - 1) && av[i][0] == '-' && !ft_isdigit(av[i][1]))
	{
		av[i]++;
		parse_options(av, i, &i, ac);
		i++;
	}
	return (i);
}

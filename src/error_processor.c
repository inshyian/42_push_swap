/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_processor.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 13:25:21 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 10:25:51 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../inc/push_swap.h"

static void		error_processor_(char *str, int code)
{
	if (code == 21)
		ft_fprintf(2, "InvalidInstruction%{normal}y (%s)\n", str);
	else if (code == 30)
		ft_fprintf(2, "InvalidFlag%{normal}y (%s)\n", str);
	else if (code == 31)
		ft_fprintf(2, "LackOfArgumentAfterFlag%{normal}y (%s)\n", str);
	else if (code == 32)
		ft_fprintf(2, "IncompatibleFlags%{normal}y (%s)\n", str);
	else if (code == 5)
		ft_fprintf(2, "InvalidInstructionsInputFileName%{normal}y (%s)\n", str);
	else if (code == 6)
		ft_fprintf(2, "InvalidInstructionsOutputFileName%{normal}y (%s)\n",
																		str);
	else if (code == 7)
		ft_fprintf(2, "InvalidStackInputFileName%{normal}y (%s)\n", str);
	else if (code == 8)
		ft_fprintf(2, "InvalidTimeDelay%{normal}y (%s)\n", str);
}

void			error_processor(char *str, int code)
{
	ft_printf("%{bold,red}yError(%{white}y%d%{red}y): %{bold,white}y", code);
	if (code == 10)
		ft_fprintf(2, "NoStack%{normal}y\n");
	else if (code == 11)
		ft_fprintf(2, "InvalidStackItem%{normal}y (%s)\n", str);
	else if (code == 12)
		ft_fprintf(2, "DuplicateStackItem%{normal}y (%s)\n", str);
	else if (code == 13)
		ft_fprintf(2, "EmptyStackItem%{normal}y\n");
	error_processor_(str, code);
	if (str)
		free(str);
	exit(0);
}

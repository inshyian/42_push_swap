/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_max_min_stack_item.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 20:19:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 11:32:07 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

char			*get_max_stack_item(t_dllist *stack)
{
	t_dllist	*first;
	char		*max;

	first = stack;
	max = stack->content;
	while (stack)
	{
		if (ft_longexprcmp(stack->content, '>', max))
			max = stack->content;
		stack = stack->next;
		if (stack == first)
			break ;
	}
	return (max);
}

char			*get_min_stack_item(t_dllist *stack)
{
	t_dllist	*first;
	char		*min;

	first = stack;
	min = stack->content;
	while (stack)
	{
		if (ft_longexprcmp(stack->content, '<', min))
			min = stack->content;
		stack = stack->next;
		if (stack == first)
			break ;
	}
	return (min);
}

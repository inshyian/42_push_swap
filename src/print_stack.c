/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_stack.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/27 12:28:58 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 14:03:55 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ncurses.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "../inc/push_swap.h"

static void		put_instr(WINDOW *win_x, t_list *instr)
{
	int				i;
	size_t			instructions_count;

	instructions_count = ft_lstlen(instr);
	i = 0;
	wclear(win_x);
	box(win_x, 0, 0);
	while (instr && i + 2 < G->ncurses.rows)
	{
		mvwaddstr(win_x, 1 + i++, 1, instr->content);
		instr = instr->next;
	}
	wrefresh(win_x);
}

static void		put_stack(WINDOW *win_x, t_dllist *stack_x, t_dllist *first)
{
	int				i;
	int				stack_size;

	i = 0;
	stack_size = ft_dllstlen(stack_x, NULL);
	wclear(win_x);
	box(win_x, 0, 0);
	while (stack_x)
	{
		if (i == G->ncurses.rows / 2 && stack_size + 2 > G->ncurses.rows)
		{
			mvwaddstr(win_x, 1 + i++, 1, "...");
			stack_size = stack_size - i;
			while (stack_size-- !=
					G->ncurses.rows / 2 - (IS_EVEN(G->ncurses.rows) ? 4 : 3))
				stack_x = stack_x->next;
		}
		mvwaddstr(win_x, 1 + i++, 1, stack_x->content);
		stack_x = stack_x->next;
		if (stack_x == first)
			break ;
	}
	wrefresh(win_x);
}

static WINDOW	*win_x(int height, int width, int offsety, int offsetx)
{
	WINDOW			*win;

	win = newwin(height, width, offsety, offsetx);
	box(win, 0, 0);
	wrefresh(win);
	return (win);
}

static void		init_ncurses(void)
{
	short			height;
	short			width;
	struct winsize	w;

	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	height = w.ws_row;
	G->ncurses.rows = height;
	width = 30;
	initscr();
	curs_set(0);
	refresh();
	G->ncurses.win_stack_a = win_x(height, width, 0, (COLS - width * 3) / 2);
	G->ncurses.win_stack_b = win_x(height, width, 0,
									(COLS - width * 3) / 2 + width);
	G->ncurses.win_instr = win_x(height, 5, 0,
									(COLS - width * 3) / 2 + width * 2);
}

void			print_stack(t_list *instruction, int mode)
{
	if (mode == 1)
		init_ncurses();
	initscr();
	curs_set(0);
	refresh();
	put_stack(G->ncurses.win_stack_a, G->stacks[0], G->stacks[0]);
	put_stack(G->ncurses.win_stack_b, G->stacks[1], G->stacks[1]);
	put_instr(G->ncurses.win_instr, instruction);
	refresh();
	if (mode == 2)
		endwin();
}

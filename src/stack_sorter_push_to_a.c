/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_sorter_push_to_a.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/19 21:58:45 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 09:38:45 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

void		stack_sorter_push_to_a(t_dllist *first, t_dllist *last,
									t_list **all_instructions)
{
	t_list		*next_instruction;
	size_t		len;
	int			curr_stack;

	len = ft_dllstlen(first, last);
	curr_stack = stack_sorter_stack_id(first);
	if (curr_stack == 1 && len <= 3)
	{
		while (len--)
		{
			next_instruction = ft_lstnew(PX(OTHER(curr_stack)), 3);
			instruction_processor(next_instruction);
			ft_lstaddend(all_instructions, next_instruction);
		}
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_instructions.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 15:38:04 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/21 19:16:58 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

static int		instruction_validator(char *instr)
{
	char		**valid_list;
	short		valid;
	int			i;

	valid = 0;
	i = 0;
	valid_list = ft_strsplit(VALID_INSTRUCTIONS, ' ');
	while (valid_list[i])
	{
		if (!ft_strcmp(instr, valid_list[i]))
			valid = 1;
		free(valid_list[i]);
		i++;
	}
	free(valid_list);
	return (valid);
}

t_list			*get_instructions(void)
{
	t_list		*instructions;
	t_list		*first;
	char		*instr;

	instructions = NULL;
	first = NULL;
	while (get_next_line(G->params.fd_iin, &instr) == 1)
	{
		if (!instruction_validator(instr))
		{
			ft_lstdel(&first, &ft_memsdel);
			ft_dllstdel(&G->stacks[0], &ft_memsdel);
			error_processor(instr, 21);
		}
		ft_lstaddend(&instructions, ft_lstnew(instr, ft_strlen(instr) + 1));
		if (!first)
			first = instructions;
		if (instructions->next)
			instructions = instructions->next;
		free(instr);
	}
	return (first);
}

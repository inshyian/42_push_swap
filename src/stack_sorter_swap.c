/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_sorter_swap.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/18 20:41:46 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/22 09:38:52 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/push_swap.h"

void			stack_sorter_swap(t_dllist **first, t_dllist **second,
									t_list **all_instructions)
{
	t_list		*next_instruction;
	int			curr_stack;

	curr_stack = stack_sorter_stack_id(*first);
	if (curr_stack == 0 && G->stacks[curr_stack])
	{
		if (compare_items(G->stacks[curr_stack], G->stacks[curr_stack]->next))
		{
			ft_pswap((void **)first, (void **)second);
			next_instruction = ft_lstnew(SX(THIS(curr_stack)), 3);
			instruction_processor(next_instruction);
			ft_lstaddend(all_instructions, next_instruction);
		}
	}
	else if (curr_stack == 1 && G->stacks[curr_stack])
	{
		if (!compare_items(G->stacks[curr_stack], G->stacks[curr_stack]->next))
		{
			ft_pswap((void **)first, (void **)second);
			next_instruction = ft_lstnew(SX(THIS(curr_stack)), 3);
			instruction_processor(next_instruction);
			ft_lstaddend(all_instructions, next_instruction);
		}
	}
}
